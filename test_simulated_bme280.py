import unittest
import simulated_bme280


class TestDummyBME280(unittest.IsolatedAsyncioTestCase):
    async def test_can_return_float_from_read_temperature(self):
        sensor = simulated_bme280.BME280()
        temperature = await sensor.read_temperature()
        self.assertIsInstance(temperature, float)

    async def test_can_create_and_return_config_byte_from_set_sensor_config(self):
        sensor = simulated_bme280.BME280()
        config_byte = await sensor.set_sensor_config({'enable_high_sensor_rate': False,
                                                      'enable_filter': False,
                                                      'enable_3wire_spi': False})
        self.assertEqual(config_byte, 0b11100000)


if __name__ == "__main__":
    unittest.main()

