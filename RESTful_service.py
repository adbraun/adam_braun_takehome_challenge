from typing import Optional
from fastapi import FastAPI, Request
from fastapi.templating import Jinja2Templates
from simulated_bme280 import BME280
from pydantic import BaseModel

app = FastAPI()
sensor = BME280()
templates = Jinja2Templates(directory='templates')


class Config(BaseModel):
    # requiring booleans limits edge cases at the expense of better control
    # I would implement better granularity in a real project
    enable_high_sensor_rate: bool = True
    enable_filter: bool = False
    enable_3wire_spi: bool = False


@app.get("/")
def home(request: Request):
    # home page shows live updating temperature readings
    # form for sensor_config not implemented to reduce time
    return templates.TemplateResponse("index.html", {"request": request})


@app.get("/api/temperature")
async def read_temperature():
    temperature = await sensor.read_temperature()
    return {"temperature": temperature}


@app.post("/api/sensor_config")
async def set_sensor_config(config: Config):
    result = await sensor.set_sensor_config(config.dict())
    return {"config_byte": result}
