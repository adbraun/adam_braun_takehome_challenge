from fastapi.testclient import TestClient
from RESTful_service import app

client = TestClient(app)


def test_response_is_200_from_get_temperature():
    response = client.get('/api/temperature')
    assert response.status_code == 200


def test_response_is_200_from_post_sensor_config():
    response = client.post('/api/sensor_config',
                           headers={'Content-Type': 'application/json'},
                           json={
                                   'enable_high_sensor_rate': True,
                                   'enable_filter': True,
                                   'enable_3wire_spi': True
                                }
                           )
    assert response.status_code == 200


def test_response_is_200_from_home():
    response = client.get('/')
    assert response.status_code == 200
