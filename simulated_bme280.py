from smbus2 import SMBus
from random import randint

# Default I2C address
BME280_I2C_ADDRESS = 0x77  # 0x76 if SDO shorted to GND, 0x77 if SDO shorted to V(DDIO)


class BME280:
    """
    Simulate BME280 sensor and provide dummy methods for driver.

    :param address: I2C address of BME280 sensor
    :type address: int

    :param bus_number: hardware bus number for I2C bus
    :type bus_number: int
    """
    def __init__(self, address=BME280_I2C_ADDRESS, bus_number=1):
        # set up I2C connection with BME280

        self._address = address

        try:
            # self._device = SMBus(bus_number)  # for real hardware
            self._device = bus_number  # dummy device
        except IOError:
            # if we cant connect to sensor, do something
            print("Could not connect to BME280. Restart program to try again")  # todo: make this better if extra time

        # fake temperature calibration values
        self.dig_T1 = float(0.0)
        self.dig_T2 = float(25.0)
        self.dig_T3 = float(15.0)
        # self.read_temp_calibration_registers()  # do this with real hardware

        # fake temperature bytes
        self.temp_byte_1 = randint(10, 255)  # chose this range for prettier fake temperature numbers
        self.temp_byte_2 = randint(0, 255)
        self.temp_byte_3 = randint(0, 15) << 4

    def read_temp_calibration_registers(self):
        """
        Reads temperature calibration registers.
        This would be used with real hardware, but only shown here for demonstration.
            0x88 / 0x89 (dig_T1 [7:0] / [15:8])  unsigned short
            0x8A / 0x8B (dig_T2 [7:0] / [15:8])  signed short
            0x8C / 0x8D (dig_T3 [7:0] / [15:8])  signed short
        :returns None: updates self.dig_T1, self.dig_T2, self.dig_T3
        """
        # smbus2 assumes little endian on read_word_data
        self.dig_T1 = self._device.read_word_data(self._address, 0x88) & 0xFFFF

        # self.dig_T2 is signed short so convert if over positive limit (using two's compliment)
        self.dig_T2 = self._device.read_word_data(self._address, 0x8A) & 0xFFFF
        if self.dig_T2 > 32767:
            self.dig_T2 -= 65536

        # self.dig_T3 is signed short so convert if over positive limit (using two's compliment)
        self.dig_T3 = self._device.read_word_data(self._address, 0x8C) & 0xFFFF
        if self.dig_T3 > 32767:
            self.dig_T3 -= 65536

        # These values need to be floats for the compensation equation
        self.dig_T1 = float(self.dig_T1)
        self.dig_T2 = float(self.dig_T2)
        self.dig_T3 = float(self.dig_T3)

    async def read_temperature(self):
        """
        First, reads 3 temperature registers from BMP280.
        Then uses calibration values to adjust temperature to human readable.
        20 bit resolution
        registers:
            0xFA (temp_msb[7:0])
            0xFB (temp_lsb[7:0])
            0xFC (temp_xlsb[3:0])
        :returns temperature (float, rounded to 2 decimals for readability)
        """

        # smbus2.read_i2c_block_data returns list of bytes
        # register_values = self._device.read_i2c_block_data(self._address, 0xFA, 3)  # for real hardware
        register_values = [self.temp_byte_1, self.temp_byte_2, self.temp_byte_3]  # use random bytes chosen in __init__

        # update our random bytes for simulation purposes
        self.temp_byte_1 = min(max(0, self.temp_byte_1 + randint(-5, 5)), 255)
        self.temp_byte_2 = min(max(0, self.temp_byte_2 + randint(-5, 5)), 255)

        # combine bytes into single number, then cast into float for compensation
        raw_temp = float((register_values[0] << 16) | (register_values[1] << 8) | (register_values[2] >> 4))

        # temperature compensation equation found in datasheet
        var1 = ((raw_temp / 16386.0) - (self.dig_T1 / 1024.0)) * self.dig_T2
        var2 = ((raw_temp / 131072.0) - (self.dig_T1 / 8192.0)) * \
               ((raw_temp / 131072.0) - (self.dig_T1 / 8192.0)) * self.dig_T3

        temperature = (var1 + var2) / 5120.0
        return round(temperature, 2)

    async def set_sensor_config(self, config_dict):
        """
        Writes to "config" register (0xF5) to set config
        input is dict with the following keys
        :param config_dict: dictionary passed in through api with the following keys
            :key enable_high_sensor_rate: bool (highest or lowest sensor update rate)
            :key enable_filter: bool (low pass filter off or on highest level)
            :key enable_3wire_spi: bool (off or on)
        :return: config_byte : int
        """

        # Turn booleans into correct bits as defined in datasheet
        if config_dict['enable_high_sensor_rate']:
            t_sb_bits = 0b000
        else:
            t_sb_bits = 0b111

        if config_dict['enable_filter']:
            filter_bits = 0b100
        else:
            filter_bits = 0b000

        if config_dict['enable_3wire_spi']:
            spi_bit = 0b1
        else:
            spi_bit = 0b0

        # combine config bits into byte

        config_byte = (t_sb_bits << 5) | (filter_bits << 2) | spi_bit

        # self._device.write_byte(self._address, 0xF5, config_byte)  # for real hardware
        return config_byte
